<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#4285f4">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>StreamViewer App</title>
    <link rel="manifest" href="/manifest.json">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,500,700,400italic|Material+Icons">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
    <div id="app">
        <app></app>
    </div>
    <script type="text/javascript">
        @php
            //$protocol = (getenv('APP_ENV') == 'production') ? 'https://' : 'http://';
            $endpoint = $app->make('url')->to('/').'/api/';
            //$endpoint = str_replace('http://', $protocol, $endpoint);
        @endphp
        var api_endpoint = '{{$endpoint}}'
    </script>
    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
