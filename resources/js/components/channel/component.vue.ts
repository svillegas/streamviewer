import Vue from "vue";
import axios from 'axios';
import { GChart } from 'vue-google-charts'
var moment = require('moment');

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('lll')
  }
})

export default {
  data() {
    return {
      domain: document.domain,
      channel: null,
      channelId: this.$route.params.id,
      orderBy: 'id',
      stats: [],
      videoStats: null,
    }
  },
  mounted() {
    this.fetchChannelInfo()
    this.fetchChannelStats()
  },
  computed:{
  },
  methods: {
    fetchChannelInfo() {
      let self = this
      axios.get(api_endpoint+'channel/'+this.channelId+'/?token='+localStorage.getItem("token"))
      .then(function(response){
        self.channel = response.data
      })

    },
    fetchChannelStats() {
      let self = this
      axios.post(api_endpoint+'channel/'+this.channelId+'/stats?token='+localStorage.getItem("token"))
      .then(function(response){
        if(response.data.author_statistics) {
          self.stats = Object.values(response.data.author_statistics);
          self.videoStats = response.data.video_statistics;
        }
      })
    },
    onSort (sort) {
      this.stats = _.orderBy(this.stats, [item => item[sort.name]], sort.type);
    }
  }
}
