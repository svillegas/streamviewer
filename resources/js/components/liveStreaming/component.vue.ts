import Vue from "vue";
import { GChart } from 'vue-google-charts'
var moment = require('moment');

export default {
  data() {
    return {
      domain: document.domain,
      video: null,
      chartData: null,
      channel: null,
      channelId: null,
      chartOptions: {
        chart: {
          title: 'Comments Per Minute By User',
          //subtitle: 'Sales, Expenses, and Profit: 2014-2017',
        }
      }
    };
  },
  mounted() {
    this.fetchVideoInfo();
  },
  methods: {
    fetchVideoInfo() {
      let self = this
      Vue.axios.get(api_endpoint+'video/'+this.$route.params.id+'/?token='+localStorage.getItem("token"))
      .then(function (response) {
        // handle success
        console.log(response);
        self.video = response.data
        self.channelId = self.video.snippet.channelId
        self.fetchChannelInfo()
        if(response.data.liveStreamingDetails && response.data.liveStreamingDetails.activeLiveChatId)
        Vue.axios.get('liveChat/'+self.channelId+'/'+response.data.liveStreamingDetails.activeLiveChatId+'/stats/?token='+localStorage.getItem("token"))
        .then(function (response) {
          console.log('chartData!!!!!!!');
          console.log(response);
          let chartData = [];
          var result = Object.keys(response.data.results).map(function(key) {
            let results = response.data.results[key];
            let minute = moment(key, "YYYY-MM-DD H:m").toDate()
            results.unshift(minute)
            return results;
          });
          response.data.authors.unshift('Minute')
          result.unshift(response.data.authors)
          console.log(result);
          self.chartData = result;
        });
      })

    },
    fetchChannelInfo() {
      let self = this
      Vue.axios.get(api_endpoint+'channel/'+this.channelId+'/?token='+localStorage.getItem("token"))
      .then(function(response){
        self.channel = response.data
      })

    },
    becomeFan() {
      let self = this
      Vue.axios.post(api_endpoint+'channel/'+this.channelId+'/fan/?token='+localStorage.getItem("token"))
      .then(function(response){
        self.channel = response.data
      })
    }
  },
  components: {
    GChart
  }
};
