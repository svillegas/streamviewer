import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import GAuth from 'vue-google-oauth2'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import Msg from 'vue-message'
import VueGoogleCharts from 'vue-google-charts'

import App from './views/App.vue';
import Dashboard from './components/Dashboard.vue';
import Home from './components/Home.vue';
import Register from './components/Register.vue';
import Login from './components/Login.vue';
import LiveStreaming from './components/liveStreaming/Template.vue';
import Channel from './components/channel/Template.vue';

Vue.use(VueRouter);
//Vue.use(VueResource);
Vue.use(VueMaterial)
Vue.use(Msg);
Vue.use(Vuex);
Vue.use(VueAxios, axios);
Vue.use(GAuth, {clientId: '684531270296-bomd4fe8sa3mbje38c03408apb1cbk3k.apps.googleusercontent.com', scope: 'https://www.googleapis.com/auth/youtube https://www.googleapis.com/auth/youtube.force-ssl https://www.googleapis.com/auth/youtube.readonly'})
Vue.use(VueGoogleCharts)

axios.defaults.baseURL = api_endpoint;

const router = new VueRouter({
  mode: 'hash',
  routes: [{
    path: '/',
    name: 'home',
    component: Home
  },{
    path: '/register',
    name: 'register',
    component: Register,
    meta: {
      auth: false
    }
  },{
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      auth: false
    }
  },{
    path: '/live-streaming/:id',
    name: 'livestreaming',
    component: LiveStreaming,
    meta: {
      auth: false
    }
  },{
    path: '/channel/:id',
    name: 'channel',
    component: Channel,
    meta: {
      auth: false
    }
  },{
    path: '/channel/:id/fansMessages',
    name: 'channel.fanMessages',
    component: Channel,
    meta: {
      auth: false
    }
  },{
    path: '/channel/:id/stats',
    name: 'channel.stats',
    component: Channel,
    meta: {
      auth: false
    }
  },{
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    meta: {
      auth: true
    }
  }]
});

Vue.router = router
import VueAuth from '@websanova/vue-auth'
Vue.use(VueAuth, {
  auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
  http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
  refreshData: { enabled: false }
});

const LOGIN = "LOGIN";
const LOGIN_SUCCESS = "LOGIN_SUCCESS";
const LOGOUT = "LOGOUT";

const store = new Vuex.Store({
  state: {
    isLoggedIn: localStorage.getItem("token")
  },
  mutations: {
    [LOGIN](state) {
      state.pending = true;
    },
    [LOGIN_SUCCESS](state) {
      state.isLoggedIn = true;
      state.pending = false;
    },
    [LOGOUT](state) {
      state.isLoggedIn = false;
    }
  },
  actions: {
    login({
      state,
      commit,
      rootState
    }, creds) {
      console.log("login...", creds);
      console.log(creds.token.data.access_token);
      const token = creds.token.data.access_token
      commit(LOGIN); // show spinner
      return new Promise((resolve, token) => {
        setTimeout(() => {
          localStorage.setItem("token", creds.token.data.access_token);
          axios.get('/auth/me?token='+creds.token.data.access_token)
          .then(function(response){
            localStorage.setItem('user',response.data)
          })
          .catch(function (error) {
            console.log(error)
            console.log('unauthenticated')
            self.signOut()
          })

          commit(LOGIN_SUCCESS);
          resolve();
        }, 1000);
      });

    },
    logout({
      commit
    }) {
      console.log('logging out')
      localStorage.removeItem("token");
      Vue.router.push('/login');
      console.log(localStorage.getItem('token'))
      commit(LOGOUT);
    }
  },
  getters: {
    isLoggedIn: state => {
      return state.isLoggedIn;
    },
    getToken: localStorage => {
      return localStorage.getItem('token')
    }
  }
});

Vue.store = store
App.router = Vue.router
App.store = Vue.store
const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
