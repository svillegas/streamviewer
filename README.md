# Stream viewer

Full Stack web application that help customer view top ranked livestreams from Youtube,  participate in the chat sessions and view historical chats and related statistics.

## Demo
http://stream-viewer-svm.herokuapp.com/#/login

## Technologies used
- Backend: Laravel (PHP Framework), Level of comfort: Very comfortable
- Frontend: Vue.js + Material Design, webpack for compilation
- Deployment: Heroku

## Assumptions and Design Choices
- Login with Google Account is done on the frontend and integrated on the backend, using JWT to mantain stateless session between both.
- On the home page, the live streams are sorted by YouTube relevance, with Gaming category by default. The user has the ability to select another category and search by query, infinite loading is integrated
- On the livestream page, I used the youtube iframe, that gives the possibility to modify the player height and width (the iframe returned on the Youtube API has fixed H&W). The chat is using the youtube iframe as well.
- On the livestream page, there's a chart that shows messages by user
- On the livestream page, users can become `Super Mega Fan`, this stores a record with a relationship between users and channels
- Once any user opens a video for the first time, the channel is saved into the db, this allows me to save the `fan` records
- Messages from `Super Mega Fans` are persisted on the db
- Users can visit the channel page, this shows a list of fans, a list of fan messages and stats (Number of messages by user, video comments, dislikes, favorites, likes, views)
- Users can sort the `Messages by Users` table
- The app is mostly responsive, there are some parts missing, and has some PWA features
- On the database there are the following models:
    - User
    - StreamingChannel
    - Fans
    - StreamingChannelMessages
- I made the decision to build a VideoServiceProvider with a YouTube Service in order to have a more SOLID implementation, that'll provide a more maintanable code and gives the possibilit to work with different service providers, also makes the code cleaner.

## Missing pieces
- I would have liked to implement the session refresh using the google refresh token, instead the app logs you out when your google token expires
- I tried to use the LiveChatMessages.insert api call to build the chat internally instead of using the iframe, but I couldn't, the lack of documentation is disturbing
- Unit testing for backend and frontend

