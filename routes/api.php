<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('google', 'Api\AuthController@google');
    Route::post('login', 'Api\AuthController@login');
    Route::post('logout', 'Api\AuthController@logout');
    Route::get('refresh', 'Api\AuthController@refresh');
    Route::get('me', 'Api\AuthController@me');

});

Route::group([
    'middleware' => 'api',
], function ($router) {
    // Video
    Route::post('videos', 'Api\StreamController@index');
    Route::get('video/{id}', 'Api\StreamController@video');
    Route::get('liveChat/{channelId}/{liveChatId}/stats', 'Api\StreamController@videoLiveStats');
    // Categories
    Route::get('categories', 'Api\StreamController@categories');
    // Channels
    Route::get('channel/{channelId}', 'Api\StreamingChannelController@read');
    Route::post('channel/{channelId}/fan', 'Api\StreamingChannelController@createFan');
    Route::get('channel/{channelId}/fan', 'Api\StreamingChannelController@createFan');
    Route::post('channel/{channelId}/stats', 'Api\StreamingChannelController@stats');
});
