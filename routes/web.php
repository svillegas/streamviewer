<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('streamviewer');
});


Route::get('/cochinero', 'CochineroController@index');
Route::post('/cochinero', 'CochineroController@index');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('google', function () {
    return view('google');
});

Route::post('auth/google/callback', 'Auth\GoogleController@handleGoogleCallback');
Route::post('api/auth/google/callback', 'Auth\GoogleController@handleGoogleCallback');
//Route::get('/stream', 'Api\StreamController@index');
Auth::routes();
