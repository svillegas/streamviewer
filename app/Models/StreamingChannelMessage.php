<?php

namespace App\Models;

use Eloquent;

class StreamingChannelMessage extends Eloquent
{
    protected $fillable = [
        'streaming_channel_id', 'user_id', 'message', 'datetime'
    ];

    public function streamingChannel()
    {
    	return $this->belongsTo(StreamingChannel::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}