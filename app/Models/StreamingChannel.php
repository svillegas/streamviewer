<?php

namespace App\Models;

use Eloquent;

class StreamingChannel extends Eloquent
{
    protected $fillable = [
        'name', 'description', 'streaming_service_channel_id'
    ];

    protected $appends = [
        'user_is_fan'
    ];

    public function fans()
    {
    	return $this->belongsToMany('App\Models\User','fans','streaming_channel_id','user_id');
	}

    public function streamingChannelMessages()
    {
        return $this->hasMany('App\Models\StreamingChannelMessage');
    }

    public function getUserIsFanAttribute()
    {

        $fan = $this->fans()->where('user_id','=',\Auth::user()->id)->first();
        return (!is_null($fan));
    }

}