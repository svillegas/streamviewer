<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Contracts\VideoHandlerInterface;
use App\Services\Video\Youtube;

class VideoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(VideoHandlerInterface::class, Youtube::class);
    }
}
