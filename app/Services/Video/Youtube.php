<?php

namespace App\Services\Video;

use App\Contracts\VideoHandlerInterface;
use Carbon\Carbon;
use App\Models\StreamingChannel;
use App\Models\StreamingChannelMessage;
use App\Models\User;

class Youtube implements VideoHandlerInterface
{
    public $client;
    public $service;

    public function __construct()
    {
        $apiKey = env('YOUTUBE_API_KEY');
        $this->client = new \Google_Client();
        $this->client->setDeveloperKey($apiKey);
        $this->service = new \Google_Service_YouTube($this->client);
    }

    public function getList($params)
    {

    }

    public function videoSearch($query = null, $page = null, $maxResults = 20, $categoryId = null)
    {
        $params = [
            'type' => 'video',
            'order' => 'relevance',
            'eventType' => 'live',
            'part' => 'snippet',
            'videoCategoryId' => $categoryId ?? 20,
            'maxResults' => $maxResults,
        ];

        if($query){
            $params['q'] = $query;
        }

        if($page){
            $params['pageToken'] = $page;
        }

        $searchResponse = $this->service->search->listSearch('id,snippet', $params);

        return $searchResponse;
    }

    public function getVideo($id)
    {
        $params = ['id' => $id];
        $video = null;
        $videos = $this->service->videos->listVideos('id,snippet,contentDetails,player,recordingDetails,statistics,status,topicDetails,liveStreamingDetails', $params);
        if ($videos->items) {
            $video = $videos->items[0];
        }
        return $video;
    }

    public function getCategories()
    {
        $categories = $this->service->videoCategories->listVideoCategories('snippet', 
        ['regionCode' => 'US']);
        return $categories;
    }

    public function getVideoStats($channelId, $data, $type)
    {
        $results = null;
        if ($type == 'authorByMinute') {
            $results = $this->getVideoStatsAuthorByMinute($channelId, $data);
        } else if ($type == 'messagesByAuthor') {
            $results = $this->getVideoStatsMessagesByAuthor($channelId, $data);
        }
        return $results;
    }

    public function getLiveMessages($liveChatId, $nextPageToken = null)
    {
        $params = [
            'maxResults' => 2000
        ];
        if($nextPageToken){
            $params['pageToken'] = $nextPageToken;
        }

        $searchResponse = $this->service->liveChatMessages->listLiveChatMessages($liveChatId,'id,snippet,authorDetails',$params);
        return $searchResponse;
    }

    public function getVideoStatsMessagesByAuthor($channelId, $data)
    {
        $params = [
            'type' => 'video',
            'eventType' => 'live',
            'part' => 'snippet',
            'channelId' => $channelId,
            'maxResults' => '5',
        ];

        $reponse = $this->service->search->listSearch('id,snippet',$params);
        $results = [];
        if($reponse->items){
            $video = $reponse->items[0];

            $params = ['id' => $video->id->videoId];
            $video = null;
            $videos = $this->service->videos->listVideos('id,snippet,contentDetails,player,recordingDetails,statistics,status,topicDetails,liveStreamingDetails', $params);
            if ($videos->items) {
                $video = $videos->items[0];
            }
            $results['video_statistics'] = $video->statistics;
            $liveChatId = $video->liveStreamingDetails->activeLiveChatId;
            $messages = [];
            $searchResponse = $this->getLiveMessages($liveChatId);
/*            while($searchResponse->items){
                usleep($searchResponse->pollingIntervalMillis*1000);
                $messages = array_merge($messages, $searchResponse->items);
                $searchResponse = $this->getLiveMessages($liveChatId, $searchResponse->nextPageToken);
                dd($searchResponse);
            }
*/
            $results['author_statistics'] = [];
            foreach($searchResponse->items as $message){
                if(!isset($results['author_statistics'][$message->snippet->authorChannelId])){
                    $results['author_statistics'][$message->snippet->authorChannelId] = [
                        'id' => count($results['author_statistics']),
                        'name' => $message->authorDetails->displayName,
                        'avatar' => $message->authorDetails->profileImageUrl,
                        'messages' => 0
                    ];
                }
                $results['author_statistics'][$message->snippet->authorChannelId]['messages'] += 1;
                $results['author_statistics'][$message->snippet->authorChannelId]['last_message_date'] = Carbon::parse($message->snippet->publishedAt)->setTimezone('PST')->format('Y-m-d H:i:s');
            }
        }
        return $results;
    }


    public function getVideoStatsAuthorByMinute($channelId, $data)
    {
        $liveChatId = $data['live_chat_id'];
        $channel = StreamingChannel::where('streaming_service_channel_id','=',$channelId)->first();
        $fans = $channel->fans;
        $fansChannelIds = [];

        foreach($fans as $fan){
            $fansChannelIds[$fan->id] = $fan['youtube_channel_id'];
        }
        $fansIds = array_flip($fansChannelIds);

/*
      try{
        $liveChatMessage = new \Google_Service_YouTube_LiveChatMessage();
        $liveChatSnippet = new \Google_Service_YouTube_LiveChatMessageSnippet();
        $liveChatSnippet->setLiveChatId($liveChatId);
        $liveChatSnippet->setType("textMessageEvent");
        $messageDetails = new \Google_Service_YouTube_LiveChatTextMessageDetails();
        $messageDetails->setMessageText("Message to type in chat");
        $liveChatSnippet->setTextMessageDetails($messageDetails);
        $liveChatMessage->setSnippet($liveChatSnippet);
        $videoCommentInsertResponse = $this->service->liveChatMessages->insert('snippet', $liveChatMessage);
        dd($videoCommentInsertResponse);
        
      } catch (\Exception $e) {
        dd($e->getMessage());
      }
*/

      $searchResponse = $this->service->liveChatMessages->listLiveChatMessages($liveChatId,'id,snippet,authorDetails',[
        'maxResults' => 200
      ]);

      $results = [];
      $authors = [];
      if (count($searchResponse->items)) {
        foreach($searchResponse->items as $message) {
            $datetime = Carbon::parse($message->snippet->publishedAt)->setTimezone('PST');
            $minute = $datetime->format('Y-m-d H:i');
            if(in_array($message->snippet->authorChannelId, $fansChannelIds)){
                StreamingChannelMessage::firstOrCreate([
                    'user_id' => $fansIds[$message->snippet->authorChannelId],
                    'streaming_channel_id' => $channel->id,
                    'datetime' => $datetime,
                    'message' => $message->snippet->textMessageDetails->messageText
                ])->save();
            }
          $results[$minute] = (isset($results[$minute])) ? $results[$minute] : $this->addMinuteData($authors);

          if (!isset($authors[$message->authorDetails->displayName])) {
            $authors[$message->authorDetails->displayName] = count($authors);
            $results = $this->addAuthor($results);
          }
          $results[$minute][$authors[$message->authorDetails->displayName]] += 1;
        }
      }

      return ['authors' => array_flip($authors), 'results' => $results];
    }

    public function sendMessage($liveChatId)
    {

    }

    private function addMinuteData($authors)
    {
      return array_fill(0,count($authors),0);
    }

    private function addAuthor($results)
    {
      foreach ($results as $k=>$result) {
        $results[$k][] = 0;
      }
      return $results;
    }
}