<?php

namespace App\Contracts;

interface VideoHandlerInterface
{
    public function getList($params);
    public function getCategories();
    public function videoSearch($query = null, $page = 1, $maxResults = 20, $categoryId = null);

}