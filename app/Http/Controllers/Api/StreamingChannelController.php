<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Contracts\VideoHandlerInterface;
use App\Models\User;
use App\Models\StreamingChannel;

class StreamingChannelController extends Controller
{
    private $videoServiceProvider;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(VideoHandlerInterface $videoServiceProvider)
    {
        $this->videoServiceProvider = $videoServiceProvider;
        $this->middleware('auth');
    }

    public function read($channelId)
    {
        $channel = StreamingChannel::with(['fans','streamingChannelMessages','streamingChannelMessages.user'])->where('streaming_service_channel_id','=',$channelId)->first();
        return response()->json($channel);
    }

    public function createFan($channelId)
    {
        $channel = StreamingChannel::where('streaming_service_channel_id','=',$channelId)->first();
        $user = \Auth::user();
        if ($channel) {
            $user->channelsFan()->attach($channel->id);
            $channel->refresh();
        }
        return response()->json($channel);
    }

    public function stats($channelId)
    {
        $stats = $this->videoServiceProvider->getVideoStats($channelId, [], 'messagesByAuthor');

        return response()->json($stats);
    }

}
