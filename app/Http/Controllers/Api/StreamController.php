<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Contracts\VideoHandlerInterface;
use App\Models\StreamingChannel;

class StreamController extends Controller
{
    private $videoServiceProvider;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(VideoHandlerInterface $videoServiceProvider)
    {
        $this->videoServiceProvider = $videoServiceProvider;
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
      // By default Gaming Category
      $category = $request->category ?? 20;
      // Search video by category and query string on video service provider
      $searchResponse = $this->videoServiceProvider->videoSearch($request->q, $request->page, $request->maxResults, $category);

      foreach($searchResponse->items as $item){
        // save channel if it doesn't already exists on the db
        StreamingChannel::firstOrNew([
          'name' => $item->snippet->channelTitle,
          'streaming_service_channel_id' => $item->snippet->channelId
        ])->save();
      }

      return response()->json($searchResponse);
    }

    public function video($id, Request $request)
    {
      // search video by id
      $video = $this->videoServiceProvider->getVideo($id);
      if($video){
        return response()->json($video);
      }
      return abort(404);
    }

    public function videoLiveStats($channelId, $liveChatId, Request $request)
    {
      // get live video stats
      $stats = $this->videoServiceProvider->getVideoStats($channelId, ['live_chat_id'=>$liveChatId],'authorByMinute');

      return response()->json($stats);
    }

    public function categories(Request $request)
    {
      // get all categories
      $categories = $this->videoServiceProvider->getCategories();
      return response()->json($categories);
    }
}
