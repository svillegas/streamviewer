<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Socialite;
use Exception;
use Tymon\JWTAuth\JWTAuth;
use App\Models\User;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register','google']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function google(Request $request)
    {
        try {
            $request = $request->code;

            $user = Socialite::driver('google')->scopes([\Google_Service_YouTube::YOUTUBE_FORCE_SSL,\Google_Service_YouTube::YOUTUBE_READONLY,\Google_Service_YouTube::YOUTUBE,\Google_Service_YouTube::YOUTUBEPARTNER])->userFromToken($request['Zi']['access_token']);

            $google_client_token = [
                'access_token' => $user->token,
                'refresh_token' => $user->refreshToken,
                'expires_in' => $user->expiresIn
            ];

            $client = new \Google_Client();
            $client->setApplicationName("StreamViewer");
            $client->setDeveloperKey(env('YOUTUBE_API_KEY'));
            $client->setAccessToken(json_encode($google_client_token));
            $client->setScopes('https://www.googleapis.com/auth/youtube');
            $client->addScope(\Google_Service_YouTube::YOUTUBE_FORCE_SSL);
            $client->addScope(\Google_Service_YouTube::YOUTUBE_READONLY);
            $client->addScope(\Google_Service_YouTube::YOUTUBE);
            $client->addScope(\Google_Service_YouTube::YOUTUBEPARTNER);

            $create['name'] = $user->getName();
            $create['email'] = $user->getEmail();
            $create['google_id'] = $user->getId();
            $create['avatar'] = $user->getAvatar();
            $create['token'] = $user->token;

            $youtubeService = new \Google_Service_YouTube($client);
            $channels = $youtubeService->channels->listChannels('id',['mine'=>true]);
            if (count($channels->items)>0) {
                $channel = $channels->items[0];
                $create['youtube_channel_id'] = $channel->id;
            }
            $userModel = new User;
            $createdUser = $userModel->addNew($create);
            //$response = Auth::loginUsingId($createdUser->id);
            $token = auth()->tokenById($createdUser->id);

            //return response()->json($response);
            return $this->respondWithToken($token);
        } catch (Exception $e) {
            dump($e->getMessage());
            echo $e->getTraceAsString();
            //return redirect('auth/google');

        }
    }


    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function guard(){
        return \Auth::Guard('api');
    }
}